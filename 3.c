/*C program to add and multiply two given matrixes*/
#include<stdio.h>

void main()
{
	int a[10][10], b[10][10], sum[10][10], mul[10][10];
	int i,j,k,arows,acolumns,brows,bcolumns;
	printf("Enter no. of rows and columns in matrix A: ");
	scanf("%d%d",&arows,&acolumns);
	printf("Enter no. of rows and columns in matrix B: ");
	scanf("%d%d",&brows,&bcolumns);
	if(arows!=brows || acolumns!=bcolumns)
	{
		printf("Matrix Addition is not possible");
		return;
	}
	else if(acolumns!=brows)
	{
		printf("Matrix Multiplication is not possible");
		return;
	}
	else
	{
		printf("Enter elements of matrix A: ");
		for(i=0;i<arows;i++)
			for(j=0;j<acolumns;j++)
				scanf("%d", &a[i][j]);

		printf("Enter elements of matrix B: ");
		for(i=0;i<brows;i++)
			for(j=0;j<bcolumns;j++)
				scanf("%d", &b[i][j]);


		//Matrix Addition
		for(i=0;i<arows;i++)
			for(j=0;j<acolumns;j++)
				sum[i][j] = a[i][j] + b[i][j];
		printf("\nResult of Matirx Addition:\n");
		for(i=0;i<arows;i++)
		{
			for(j=0;j<acolumns;j++)
				printf("%d ", sum[i][j]);
			printf("\n");
		}


		//Matrix Multiplication
		int total=0;
		for(i=0;i<arows;i++)
			for(j=0;j<bcolumns;j++)
                {
				for(k=0;k<brows;k++)
                    {
					total += a[i][k]*b[k][j];
				    }
                    mul[i][j]=total;
                    total =0;
                }
		printf("\nResult of Matirx Multiplication:\n");
		for(i=0;i<arows;i++)
		{
			for(j=0;j<bcolumns;j++)
				printf("%d ", mul[i][j]);
			printf("\n");
		}
	}

}
